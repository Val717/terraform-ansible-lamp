#The Region you want to deploy the infra
aws_region         = "eu-central-1"

# Replace the VPC cidr from your account. Range of CIDR max(22)
vpc_cidr           = "172.168.0.0/16"

# Key pair name create in same region
key_name           = "ValAWS"

# personal laptop public ip for debugging and SSH purpose
personal_laptop_ip = "77.85.210.139"

